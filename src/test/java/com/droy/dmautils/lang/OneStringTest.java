package com.droy.dmautils.lang;

import org.junit.Test;

import static org.junit.Assert.*;

public class OneStringTest {

    @Test
    public void equalsTest() {
        OneString string1 = new OneString("geeky");
        assertTrue( string1.equals("geeky"));

        OneString string2 = new OneString("CitiBank India");
        OneString string3 = new OneString("CitiBank India");
        assertTrue( string2.equals(string3));

        OneString string4 = new OneString("CitiBank India Ltd");
        assertFalse(string3.equals(string4));
    }

    @Test
    public void compareTo(){
        OneString string1 = new OneString("geeky");
        assertTrue( string1.compareTo("geeky") == 0);

        OneString string2 = new OneString("CitiBank India");
        OneString string3 = new OneString("CitiBank India");
        assertTrue(string2.compareTo(string3) == 0);

        OneString string4 = new OneString("CitiBank India Ltd");
        assertTrue(string3.compareTo(string4) < 0);
    }

}