package com.droy.dmautils.lang;

public class ReusableOneString extends OneString implements Reusable {
    private static final int DEFAULT_LEN = 16;

    public ReusableOneString() {
        this(DEFAULT_LEN);
    }

    public ReusableOneString(final int capacity) {
        super(new byte[capacity],0,0);
    }

    public ReusableOneString(final OneString other){
        this(other._buffer, 0, other._length);
    }

    public ReusableOneString(final byte[] bytes, final int from, final int len) {
        super(bytes, from, len);
        System.arraycopy(bytes, from, _buffer, 0, len);
    }

    public ReusableOneString( final String str) {
        super(str);
    }

    @Override
    public void reset() {
        _length = 0;
    }
}
