package com.droy.dmautils.lang;

public class OneString implements Comparable<OneString> {

    private static final int DEFAULT_ONE_STRING_LEN = 16;
    protected byte[]  _buffer;
    protected int     _length;

    public OneString() {
        _buffer = new byte[DEFAULT_ONE_STRING_LEN];
        _length = 0;
    }

    public OneString(int initialCapacity) {
        _buffer = new byte[initialCapacity];
        _length = 0;
    }

    public OneString(String str) {
        if (str == null) {
            _buffer = new byte[0];
            _length = 0;
        }
        else {
            _buffer = str.getBytes();
            _length = str.length();
        }
    }

    public OneString(OneString oneString) {
        if (oneString == null) {
            _buffer = new byte[0];
            _length = 0;
        }
        else {
            _buffer = oneString._buffer;
            _length = oneString._length;
            System.arraycopy(oneString._buffer,0, _buffer, 0, _length);
        }
    }

    public OneString( byte[] buffer, int start, int length) {
        _length = length;
        _buffer = new byte[length];
        System.arraycopy(buffer, 0, _buffer, 0, length);
    }

    public boolean isEmpty() {
        return _length == 0;
    }

    public int length() {
        return _length;
    }

    public char charAt(int i) {
        return (char)_buffer[i];
    }

    public final boolean equals(final OneString other) {
        if (this == other) {
            return true;
        }

        if (other == null ) {
            return false;
        }

        final int len = other._length;
        if (_length != len) {
            return false;
        }

        final byte buf2[] = other._buffer;
        for (int i =0; i < _length; i++ ) {
            byte b1 = _buffer[i];
            byte b2 = buf2[i];
            if (b1 != b2) {
                return false;
            }
        }

        return true;
    }

    public final boolean equals( final String other) {
        if (other == null ) {
            return false;
        }

        final int len = other.length();
        if (_length != len) {
            return false;
        }

        final byte buf2[] = other.getBytes();
        for (int i =0; i < _length; i++ ) {
            byte b1 = _buffer[i];
            byte b2 = buf2[i];
            if (b1 != b2) {
                return false;
            }
        }

        return true;
    }



    @Override
    public int compareTo(OneString oneString) {
        int len = oneString._length;
        byte[] buffer = oneString._buffer;

        for (int i =0; i < _length; i++ ) {
            byte b1 = _buffer[i];
            byte b2 = buffer[i];
            if (b1 != b2) {
                return b1 - b2;
            }
        }

        if (_length != len) {
            return (_length > len) ? 1 : -1;
        }
        return 0;
    }

    public int compareTo(String other) {
        int len = other.length();
        byte[] buffer = other.getBytes();

        for (int i =0; i < _length; i++ ) {
            byte b1 = _buffer[i];
            byte b2 = buffer[i];
            if (b1 != b2) {
                return b1 - b2;
            }
        }

        if (_length != len) {
            return (_length > len) ? 1 : -1;
        }
        return 0;
    }
}
