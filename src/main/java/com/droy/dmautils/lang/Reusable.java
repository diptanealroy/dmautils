package com.droy.dmautils.lang;

public interface Reusable {
    public void reset();
}
